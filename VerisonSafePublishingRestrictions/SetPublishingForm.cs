﻿using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Globalization;
using Sitecore.Text;
using Sitecore.Web;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Web.UI.Pages;
using Sitecore.Web.UI.Sheer;
using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VerisonSafePublishingRestrictions
{
    /// <summary/>
    public class SetPublishingForm : DialogForm
    {
        /// <summary/>
        protected Border Warning;

        /// <summary/>
        protected Scrollbox Versions;

        /// <summary/>
        protected Border PublishPanel;

        /// <summary/>
        protected Checkbox NeverPublish;

        /// <summary/>
        protected DateTimePicker Publish;

        /// <summary/>
        protected DateTimePicker Unpublish;

        /// <summary/>
        protected Border PublishingTargets;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="T:Sitecore.Shell.Applications.ContentManager.Dialogs.SetPublishing.SetPublishingForm"/> reads the only.
        /// 
        /// </summary>
        /// 
        /// <value>
        /// <c>true</c> if the <see cref="T:Sitecore.Shell.Applications.ContentManager.Dialogs.SetPublishing.SetPublishingForm"/> reads the only; otherwise, <c>false</c>.
        /// 
        /// </value>
        private bool ReadOnly
        {
            get { return MainUtil.GetBool(this.ServerProperties["ReadOnly"], false); }
            set { this.ServerProperties["ReadOnly"] = (object) (value ? 1 : 0); }
        }

        /// <summary>
        /// Raises the load event.
        /// 
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        /// <remarks>
        /// This method notifies the server control that it should perform actions common to each HTTP
        ///             request for the page it is associated with, such as setting up a database query. At this
        ///             stage in the page lifecycle, server controls in the hierarchy are created and initialized,
        ///             view state is restored, and form controls reflect client-side data. Use the IsPostBack
        ///             property to determine whether the page is being loaded in response to a client postback,
        ///             or if it is being loaded and accessed for the first time.
        /// 
        /// </remarks>
        protected override void OnLoad(EventArgs e)
        {
            Assert.ArgumentNotNull((object) e, "e");
            base.OnLoad(e);
            if (Context.ClientPage.IsEvent)
                return;
            if (WebUtil.GetQueryString("ro") == "1")
                this.ReadOnly = true;
            Item itemFromQueryString = UIUtil.GetItemFromQueryString(Context.ContentDatabase);
            Error.AssertItemFound(itemFromQueryString);
            this.RenderItemTab(itemFromQueryString);
            this.RenderVersions(itemFromQueryString);
            this.RenderTargetTab(itemFromQueryString);
            if (!this.ReadOnly)
                return;
            this.SetReadonly();
        }

        /// <summary>
        /// Handles a click on the OK button.
        /// 
        /// </summary>
        /// <param name="sender"/><param name="args"/>
        /// <remarks>
        /// When the user clicks OK, the dialog is closed by calling
        ///             the <see cref="M:Sitecore.Web.UI.Sheer.ClientResponse.CloseWindow">CloseWindow</see> method.
        /// </remarks>
        protected override void OnOK(object sender, EventArgs args)
        {
            Assert.ArgumentNotNull(sender, "sender");
            Assert.ArgumentNotNull((object) args, "args");
            Item itemFromQueryString = UIUtil.GetItemFromQueryString(Context.ContentDatabase);
            Error.AssertItemFound(itemFromQueryString);
            ListString listString = new ListString();
            using (new StatisticDisabler(StatisticDisablerState.ForItemsWithoutVersionOnly))
            {
                itemFromQueryString.Editing.BeginEdit();
                itemFromQueryString.Publishing.NeverPublish = !this.NeverPublish.Checked;
                itemFromQueryString.Publishing.PublishDate = DateUtil.ParseDateTime(this.Publish.Value,
                    DateTime.MinValue);
                itemFromQueryString.Publishing.UnpublishDate = DateUtil.ParseDateTime(this.Unpublish.Value,
                    DateTime.MaxValue);
                foreach (string text in Context.ClientPage.ClientRequest.Form.Keys)
                {
                    if (text != null && text.StartsWith("pb_", StringComparison.InvariantCulture))
                    {
                        string str = ShortID.Decode(StringUtil.Mid(text, 3));
                        listString.Add(str);
                    }
                }
                itemFromQueryString[FieldIDs.PublishingTargets] = listString.ToString();
                itemFromQueryString.Editing.EndEdit();
            }
            Log.Audit((object) this, "Set publishing targets: {0}, targets: {1}",
                AuditFormatter.FormatItem(itemFromQueryString), listString.ToString());
            foreach (string text in Context.ClientPage.ClientRequest.Form.Keys)
            {
                if (text != null && text.StartsWith("pb_", StringComparison.InvariantCulture))
                {
                    string str = ShortID.Decode(StringUtil.Mid(text, 3));
                    listString.Add(str);
                }
            }
            foreach (Item obj in itemFromQueryString.Versions.GetVersions())
            {
                bool b = StringUtil.GetString(new string[1]
                {
                    Context.ClientPage.ClientRequest.Form["hide_" + (object) obj.Version.Number]
                }).Length <= 0;
                DateTimePicker dateTimePicker1 =
                    this.Versions.FindControl("validfrom_" + (object) obj.Version.Number) as DateTimePicker;
                DateTimePicker dateTimePicker2 =
                    this.Versions.FindControl("validto_" + (object) obj.Version.Number) as DateTimePicker;
                Assert.IsNotNull((object) dateTimePicker1, "Version valid from datetime picker");
                Assert.IsNotNull((object) dateTimePicker2, "Version valid to datetime picker");

                // Modify Here

                if (string.IsNullOrEmpty(dateTimePicker1.Value) && string.IsNullOrEmpty(dateTimePicker2.Value))
                {
                    continue;
                }

                DateTime t1_1 = DateUtil.IsoDateToDateTime(dateTimePicker1.Value, DateTime.MinValue);
                DateTime t1_2 = DateUtil.IsoDateToDateTime(dateTimePicker2.Value, DateTime.MaxValue);
                if (b != obj.Publishing.HideVersion ||
                    DateUtil.CompareDatesIgnoringSeconds(t1_1, obj.Publishing.ValidFrom) != 0 ||
                    DateUtil.CompareDatesIgnoringSeconds(t1_2, obj.Publishing.ValidTo) != 0)
                {
                    obj.Editing.BeginEdit();
                    obj.Publishing.ValidFrom = t1_1;
                    obj.Publishing.ValidTo = t1_2;
                    obj.Publishing.HideVersion = b;
                    obj.Editing.EndEdit();
                    Log.Audit((object) this, "Set publishing valid: {0}, from: {1}, to:{2}, hide: {3}",
                        AuditFormatter.FormatItem(obj), t1_1.ToString(), t1_2.ToString(), MainUtil.BoolToString(b));
                }

            }
            SheerResponse.SetDialogValue("yes");
            base.OnOK(sender, args);
        }

        /// <summary>
        /// Sets the never publish.
        /// 
        /// </summary>
        protected void SetNeverPublish()
        {
            bool isDisabled = !this.NeverPublish.Checked;
            this.Publish.Disabled = isDisabled;
            this.Unpublish.Disabled = isDisabled;
            Item itemFromQueryString = UIUtil.GetItemFromQueryString(Context.ContentDatabase);
            Error.AssertItemFound(itemFromQueryString);
            if (Context.ClientPage.IsEvent)
            {
                foreach (Item obj in itemFromQueryString.Versions.GetVersions())
                    this.UpdateVersionState(obj.Version.Number, isDisabled);
            }
            Context.ClientPage.ClientResponse.Refresh((System.Web.UI.Control) this.Publish);
            Context.ClientPage.ClientResponse.Refresh((System.Web.UI.Control) this.Unpublish);
        }

        /// <summary>
        /// Updates the state of the version.
        /// 
        /// </summary>
        /// <param name="versionNumber">The version number.</param><param name="isDisabled">if set to <c>true</c> version is disabled.</param>
        private void UpdateVersionState(int versionNumber, bool isDisabled)
        {
            string str = isDisabled ? "true" : "false";
            SheerResponse.SetAttribute("hide_" + (object) versionNumber, "disabled", str);
            this.ChangeDateTimePickerState((Sitecore.Web.UI.HtmlControls.Control) this.Versions,
                "validto_" + (object) versionNumber, isDisabled);
            this.ChangeDateTimePickerState((Sitecore.Web.UI.HtmlControls.Control) this.Versions,
                "validfrom_" + (object) versionNumber, isDisabled);
        }

        /// <summary>
        /// Changes the state of the date time picker.
        /// 
        /// </summary>
        /// <param name="parent">The parent.</param><param name="controlId">The control identifier.</param><param name="state">if set to <c>true</c> control will be disabled.</param>
        private void ChangeDateTimePickerState(Sitecore.Web.UI.HtmlControls.Control parent, string controlId, bool state)
        {
            DateTimePicker dateTimePicker = parent.FindControl(controlId) as DateTimePicker;
            if (dateTimePicker == null)
                return;
            dateTimePicker.Disabled = state;
            Context.ClientPage.ClientResponse.Refresh((System.Web.UI.Control) dateTimePicker);
        }

        /// <summary>
        /// Renders the item tab.
        /// 
        /// </summary>
        /// <param name="item">The item.</param>
        private void RenderItemTab(Item item)
        {
            Assert.ArgumentNotNull((object) item, "item");
            this.NeverPublish.Checked = !item.Publishing.NeverPublish;
            this.Publish.Value = item.Publishing.PublishDate == DateTime.MinValue
                ? string.Empty
                : DateUtil.ToIsoDate(item.Publishing.PublishDate);
            this.Unpublish.Value = item.Publishing.UnpublishDate == DateTime.MaxValue
                ? string.Empty
                : DateUtil.ToIsoDate(item.Publishing.UnpublishDate);
            this.SetNeverPublish();
        }

        /// <summary>
        /// Renders the target tab.
        /// 
        /// </summary>
        /// <param name="item">The item.</param>
        private void RenderTargetTab(Item item)
        {
            Assert.ArgumentNotNull((object) item, "item");
            Field field = item.Fields[FieldIDs.PublishingTargets];
            if (field == null)
                return;
            Item obj1 = Context.ContentDatabase.Items["/sitecore/system/publishing targets"];
            if (obj1 == null)
                return;
            StringBuilder stringBuilder = new StringBuilder();
            string str1 = field.Value;
            foreach (Item obj2 in obj1.Children)
            {
                string str2 = str1.IndexOf(obj2.ID.ToString(), StringComparison.InvariantCulture) >= 0
                    ? " checked=\"true\""
                    : string.Empty;
                string str3 = string.Empty;
                if (this.ReadOnly)
                    str3 = " disabled=\"true\"";
                stringBuilder.Append("<input id=\"pb_" + ShortID.Encode(obj2.ID) + "\" name=\"pb_" +
                                     ShortID.Encode(obj2.ID) + "\" class=\"scRibbonCheckbox\" type=\"checkbox\"" + str2 +
                                     str3 + " style=\"vertical-align:middle\"/>");
                stringBuilder.Append(obj2.DisplayName);
                stringBuilder.Append("<br/>");
            }
            this.PublishingTargets.InnerHtml = ((object) stringBuilder).ToString();
        }

        /// <summary>
        /// Renders the versions.
        /// 
        /// </summary>
        /// <param name="item">The item.</param>
        private void RenderVersions(Item item)
        {
            Assert.ArgumentNotNull((object) item, "item");
            Item[] versions = item.Versions.GetVersions();
            StringBuilder stringBuilder1 =
                new StringBuilder("<table width=\"100%\" cellpadding=\"4\" cellspacing=\"0\">");
            stringBuilder1.Append("<col align=\"right\" />");
            stringBuilder1.Append("<col align=\"center\" />");
            stringBuilder1.Append("<col />");
            stringBuilder1.Append("<col />");
            stringBuilder1.Append("<tr style=\"background:#e9e9e9\">");
            stringBuilder1.Append("<td nowrap=\"nowrap\"><b>" + Translate.Text("Version") + "</b></td>");
            stringBuilder1.Append("<td nowrap=\"nowrap\"><b>" + Translate.Text("Publishable") + "</b></td>");
            stringBuilder1.Append("<td width=\"50%\"><b>" + Translate.Text("Publishable From") + "</b></td>");
            stringBuilder1.Append("<td width=\"50%\"><b>" + Translate.Text("Publishable To") + "</b></td>");
            stringBuilder1.Append("</tr>");
            this.Versions.Controls.Add((System.Web.UI.Control) new LiteralControl(((object) stringBuilder1).ToString()));
            string str1 = string.Empty;
            bool flag = item.Publishing.NeverPublish || this.ReadOnly || !item.Access.CanWriteLanguage() ||
                        !this.NeverPublish.Checked;
            if (flag)
                str1 = " disabled=\"true\"";
            for (int index = 0; index < versions.Length; ++index)
            {
                StringBuilder stringBuilder2 = new StringBuilder();
                Item obj = versions[index];
                string str2 = obj.Version.Number.ToString();
                string str3 = obj.Version == item.Version ? " style=\"background:#cedff2\"" : string.Empty;
                stringBuilder2.Append("<tr" + str3 + ">");
                stringBuilder2.AppendFormat("<td><b>{0}.</b></td>", (object) obj.Version.Number);
                stringBuilder2.AppendFormat(
                    "<td><input id=\"hide_" + str2 + "\" type=\"checkbox\"" +
                    (obj.Publishing.HideVersion ? string.Empty : " checked=\"checked\"") + str1 + "/></td>",
                    new object[0]);
                stringBuilder2.Append("<td>");
                this.Versions.Controls.Add(
                    (System.Web.UI.Control) new LiteralControl(((object) stringBuilder2).ToString()));
                DateTimePicker dateTimePicker1 = new DateTimePicker();
                dateTimePicker1.ID = "validfrom_" + str2;
                dateTimePicker1.Width = new Unit(100.0, UnitType.Percentage);
                dateTimePicker1.Value = obj.Publishing.ValidFrom == DateTime.MinValue
                    ? string.Empty
                    : DateUtil.ToIsoDate(obj.Publishing.ValidFrom);
                this.Versions.Controls.Add((System.Web.UI.Control) dateTimePicker1);
                this.Versions.Controls.Add((System.Web.UI.Control) new LiteralControl("</td><td>"));
                DateTimePicker dateTimePicker2 = new DateTimePicker();
                dateTimePicker2.ID = "validto_" + str2;
                dateTimePicker2.Width = new Unit(100.0, UnitType.Percentage);
                dateTimePicker2.Value = obj.Publishing.ValidTo == DateTime.MaxValue
                    ? string.Empty
                    : DateUtil.ToIsoDate(obj.Publishing.ValidTo);
                this.Versions.Controls.Add((System.Web.UI.Control) dateTimePicker2);
                if (flag)
                {
                    dateTimePicker2.Disabled = true;
                    dateTimePicker1.Disabled = true;
                }
                this.Versions.Controls.Add((System.Web.UI.Control) new LiteralControl("</td></tr>"));
            }
            this.Versions.Controls.Add((System.Web.UI.Control) new LiteralControl("</table>"));
        }

        /// <summary>
        /// Sets the readonly.
        /// 
        /// </summary>
        private void SetReadonly()
        {
            this.ReadOnly = true;
            this.NeverPublish.Disabled = true;
            this.Publish.Disabled = true;
            this.Unpublish.Disabled = true;
            this.Warning.Attributes.Remove("hidden");
        }
    }
}